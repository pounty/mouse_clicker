using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Mouse_Clicker
{
    public partial class Form1 : Form
    {
        // DLL f�r Mouse Control einbinden
        [DllImport("user32.dll")]
        static extern void mouse_event(int dwFlags, int dx, int dy,
                               int dwData, int dwExtraInfo);

        // Enumeration f�r Mausinteraktionsadressen 
        public enum MouseActionAdresses
        {
            LEFTDOWN = 0x00000002,
            LEFTUP = 0x00000004,
            MIDDLEDOWN = 0x00000020,
            MIDDLEUP = 0x00000040,
            MOVE = 0x00000001,
            ABSOLUTE = 0x00008000,
            RIGHTDOWN = 0x00000008,
            RIGHTUP = 0x00000010
        }

        public Form1()
        {
            // Titel / Texte / Eigenschaften initialisieren
            InitializeComponent();
        }

        public void LeftClick(int x, int y)
        {
            // Posiition des Cursors setzen
            Cursor.Position = new System.Drawing.Point(x, y);

            // Mouse Leftdown Event ausf�hren
            mouse_event((int)(MouseActionAdresses.LEFTDOWN), 0, 0, 0, 0);
            // Mouse Lefup Event ausf�hren
            mouse_event((int)(MouseActionAdresses.LEFTUP), 0, 0, 0, 0);
        }

        // Timer starten
        private void button1_Click(object sender, EventArgs e)
        {
            // Timer proaktiv stoppen
            timer1.Stop();

            try
            {
                // Timer Intervall setzen
                // Wichtig:
                // Falls die Konvertierung fehlschl�gt wird catch ausgef�hrt
                timer1.Interval = Convert.ToInt32(textBox1.Text) * 1000;

                // Timer aktivieren
                timer1.Enabled = true;
                // Timer starten
                timer1.Start();
            }
            catch
            {
                // Dialog zur Information
                MessageBox.Show("Fehler beim Intervall! Nur Zahlen eintragen.", "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        // Event wird ausgef�hrt, wenn der Timer abgelaufen ist
        private void timer1_Tick(object sender, EventArgs e)
        {
            // Methode LeftClick / Linksklick ausf�hren
            LeftClick(0, 0);
        }

        // Timer stoppen
        private void button2_Click(object sender, EventArgs e)
        {
            // Timer stoppen
            timer1.Stop();
            // Timer deaktivieren
            timer1.Enabled = false;
        }
    }
}