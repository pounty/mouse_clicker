﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Mouse_Clicker
{
    static class Program
    {
        /// <summary>
        /// Mouse Clicker
        /// (c) 2012 Philipp Lunz
        /// E-Mail: dev@lunz.biz
        /// http://pdev.it
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}